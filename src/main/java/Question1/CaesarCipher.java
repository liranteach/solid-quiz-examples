package Question1;

public class CaesarCipher implements Cipher, CipherDbHandler {

    private final InMemoryDB ciphersDB = new InMemoryDB();
    private final int numberOfRotations;
    LetterRotator letterRotator = new LetterRotator();

    public CaesarCipher(int numberOfRotations) {
        this.numberOfRotations = numberOfRotations;
    }

    @Override
    public String encrypt(String textToEncrypt) {
        return generateCipherResult(textToEncrypt, this.numberOfRotations);
    }

    @Override
    public String decrypt(String textToDecrypt) {
        return generateCipherResult(textToDecrypt, -this.numberOfRotations);
    }

    @Override
    public String generateCipherResult(String textToCipher, int numberOfRotations){
        StringBuilder encryptedText = new StringBuilder();
        for (char letter : textToCipher.toCharArray()) {
            encryptedText.append(letterRotator.rotateLetter(letter,numberOfRotations));
        }
        return encryptedText.toString();
    }

    //Ignore this method :)
    @Override
    public char generateCipherLetter(char letterToCipher, int replaceLetter, int numberOfRotations){
        return ' ';
    }

    @Override
    public void updateCipher(String text) {
        this.ciphersDB.update(text);
    }

    @Override
    public void loadCipher(String text) {
        this.ciphersDB.load(text);
    }

    @Override
    public void deleteCipher(int textId) {
        this.ciphersDB.delete(textId);

    }
}
